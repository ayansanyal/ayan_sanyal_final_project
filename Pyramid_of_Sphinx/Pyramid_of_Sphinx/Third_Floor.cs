﻿using System;

namespace Pyramid_of_Sphinx
{
	public class Third_Floor
	{
		static public void Third_Floor_ER ()
		{
			Console.Clear ();
			Console.WriteLine ("The player enters the eastern room on the first floor\n");
			Console.WriteLine ("Lo and Behold! The Sphinx appears and asks you a riddle in it's sweet but chilling voice\n");
			Console.WriteLine ("   Voiceless it cries,\n"+
				"   Wingless flutters,\n"+
				"   Toothless bites\n"+
				"   Mouthless mutters.\n");

			Console.WriteLine ("Choose wisely as a wrong answer can lead to horrific results:\n"+
				"1. The Wind\n" +
				"2. The Light\n" +
				"3. I don't know\n");
			ConsoleKey button = Console.ReadKey ().Key;	
			Console.WriteLine ();
			switch (button) 
			{
			case(ConsoleKey.D1):
				{
					
					Console.WriteLine ("That was the correct answer and the Sphinx vanishes leaving chamber\n");
					Console.WriteLine ("Press 1 to go to lower floor");
					char key = Console.ReadKey ().KeyChar;
					if (key == '1') {
						Third_Floor_WR ();
					}
					
					break;
					}



			case(ConsoleKey.D2):
				{
					
					Console.Clear();
					Console.WriteLine ("The Sphinx gobbles the player up but after a few hours then the player wakes up in front of the Sphinx again\n");
					Console.ReadKey ();
					Third_Floor_ER ();
					break;


					
				}

			case(ConsoleKey.D3):
				{
					
					Console.WriteLine ("The Sphinx gobbles the player up but after a few hours then the player wakes up in front of the Sphinx again\n");
					Console.ReadKey();
					Third_Floor_ER ();
					break;
				}

			default:
				{
					
					Console.Clear();
					Console.WriteLine ("INVALID INPUT - Press enter to get menu again!\n\n");
					Console.ReadKey ();
					Third_Floor_ER ();
					break;
				}
			}
			
		}
	
		static public void Third_Floor_WR ()
		{
			Console.Clear ();
			Console.WriteLine ("The player enters the western room on this floor and it is quite a mess, if properly search one might find something useful\n");
			Console.WriteLine ("Press 1 to search behind the cupboard");
			Console.WriteLine ("Press 2 to search in the sacs");
			Console.WriteLine ("Press 3 to search in the crypts");
			Console.WriteLine ("Press 4 to go back to previous room");
			ConsoleKey button = Console.ReadKey ().Key;	
			Console.WriteLine ();
			switch (button) 
			{
			case(ConsoleKey.D1):
				{
					
					Console.WriteLine ("The player finds nothing but a bunch of spiders");
					Console.ReadKey ();
					Third_Floor_WR ();

						break;
					}



					case(ConsoleKey.D2):
					{
					Console.WriteLine ("The player finds nothing but a bunch of spiders");
					Console.ReadKey ();
					Third_Floor_WR ();

					break;



					}

					case(ConsoleKey.D3):
					{
						///if number key 3 was pressed lower chamber opens
						
						Console.WriteLine ("A hidden door opens up in the floor leading to a chamber below\n");
					Console.WriteLine ("Press 1 to go below\n");
					char key = Console.ReadKey ().KeyChar;
					if (key == '1') {
						Second_Floor.Second_Floor_WR ();
					}

						
						break;
					}
			case(ConsoleKey.D4):
				{
					
					Third_Floor_ER ();

					break;
				}

					default:
					{
						///the player pressed a differnt key, so we restart the function from the beginning
						Console.Clear();
						Console.WriteLine ("INVALID INPUT - PLEASE TRY AGAIN !\n\n");
						Third_Floor_WR ();
						break;
					}
				}
		}
}
}



