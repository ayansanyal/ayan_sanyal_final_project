﻿using System;

namespace Pyramid_of_Sphinx
{
	public class Second_Floor
	{
		static bool m_sword = false;
		static public void Second_Floor_WR()
		{
			
			Console.Clear ();
			Console.WriteLine ("The player now slides down to the floor below and this room is quite a mess too\n");
			Console.WriteLine ("One could find something useful here if searched properly\n");
			Console.WriteLine ("Press 1 to search under the rugs\n");
			Console.WriteLine ("Press 2 to search behind the mummy\n");
			Console.WriteLine ("Press 3 to search in the jars\n");
			Console.WriteLine ("Press 4 to enter the next room\n");
			Console.WriteLine ("Press 5 to go back to previous room\n");
			ConsoleKey button = Console.ReadKey ().Key;	
			Console.WriteLine ();
			switch (button) 
			{
			case(ConsoleKey.D1):
				{
					Console.WriteLine ("The player finds nothing but a bunch of spiders");
					Console.ReadKey ();
					Second_Floor_WR();

					break;
				}



			case(ConsoleKey.D2):
				{
					Console.WriteLine ("The player finds a golden sword gleaming with some kind of energy\n");

					m_sword = true;
					Console.WriteLine ("Press 1 to continue into next room");
					char key2 = Console.ReadKey ().KeyChar;
					if (key2 == '1') {
						Second_Floor_MR ();
					}



					break;



				}

			case(ConsoleKey.D3):
				{
					
					Console.WriteLine ("The player finds nothing but a bunch of spiders\n");
					Console.ReadKey ();
					Second_Floor_WR ();

						break;
					}
			case(ConsoleKey.D4):
				{
					///if number key 4 previous room is where they go

					Second_Floor_MR();

					break;
				}
			case(ConsoleKey.D5):
				{
					///if number key 5 next floor is where they go

					Third_Floor.Third_Floor_WR ();

					break;
				}

					default:
					{
						///the player pressed a differnt key, so we restart the function from the beginning
						Console.Clear();
						Console.WriteLine ("INVALID INPUT - Press enter to get menu again\n\n");
					Console.ReadKey ();
					Second_Floor_WR();
						break;
					}
				}

		}
		static public void Second_Floor_MR()
		{
			Console.Clear ();
			Console.WriteLine ("The player enter the room and finds a bunch of mummies waiting to attack");
			Console.WriteLine ("What should the player do?\n");
			Console.WriteLine ("Press 1 to attack them\n");
			Console.WriteLine ("Press 2 to go back to previous room\n");
			char key = Console.ReadKey ().KeyChar;
			if (key == '1' && !m_sword)
			{
				Console.WriteLine ("The player fights the mummies barehanded and ends up dying");
				Console.WriteLine ("GAME OVER\n\n\n");
				Pyramid.Main();
			}
			else if (key == '1' && m_sword )
			{
				Console.WriteLine ("The player attacks the mummies with the golden sword and they all turn to dust\n");
				Console.WriteLine ("The coast is clear and the player can proceed\n");
				Console.WriteLine ("Press 1 to proceed to next room\n");
				char key1 = Console.ReadKey ().KeyChar;
				if (key1 == '1') {
					Second_Floor_ER ();
				}
			}
			else if (key == '2')
			{
				Second_Floor_WR();
			}
		}

		static public void Second_Floor_ER ()
		{
			Console.Clear ();
			Console.WriteLine ("The player enters the room full of gleaming light which seems to come from a huge pile of gold and other treasures\n");
			Console.WriteLine ("On top of the pile sits the Sphinx who says in its shrill voice");
			Console.WriteLine ("Answer this last riddle correctly and all this gold shall be yours, but answer it wrong and you shall remain in my sotmach forever" );
			Console.WriteLine ("    When I am young,\n"+ 
				"    I go on four legs,\n"+
				"    then I go on two legs,\n"+ 
				"    then I go on three legs,\n"+ 
				"    and then I die.\n");
			Console.WriteLine ("Answer wisely for this is the final challenge");
			Console.WriteLine ("1. A goat");
			Console.WriteLine ("2. A Man");
			Console.WriteLine ("3. The sun");
			ConsoleKey button = Console.ReadKey ().Key;	
			Console.WriteLine ();
			switch (button) 
			{
			case(ConsoleKey.D1):
				{
					
					Console.Clear();
					Console.WriteLine ("False answer!!! The Sphinx jumps at the player  and eats them alive!!\n");
					Console.WriteLine ("GAME OVER\n\n\n");
					Pyramid.Main();
					break;

				}

			case(ConsoleKey.D2):
				{

					Console.WriteLine ("That was the correct answer and the Sphinx changes into a golden statue! All this gold now belongs to the player as a door in the far end open with fresh air seeping in\n");
					Console.WriteLine ("Thank you for playing!\n");
					Console.WriteLine ("Created by Ayan Sanyal\n");



					break;
				}

			case(ConsoleKey.D3):
				{
					Console.WriteLine ("False answer!!! The Sphinx jumps at the player  and eats them alive!!\n");
					Console.WriteLine ("GAME OVER\n\n\n");
					Pyramid.Main();
					break;
				}

			default:
				{
					
					Console.Clear();
					Console.WriteLine ("INVALID INPUT - Press Enter to go back to menu\n\n");
					Console.ReadKey ();
					Second_Floor_ER ();
					break;
		}
	}
}
	}
}


