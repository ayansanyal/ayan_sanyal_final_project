﻿using System;

namespace Pyramid_of_Sphinx
{
	public class Game_Start
	{
		
		//Main game starts here
		static public void Top()
		{
			Console.Clear ();
			Console.WriteLine ("Lo and Behold! The Sphinx appears and asks you a riddle in it's sweet but chilling voice\n");
			Console.WriteLine ("   It cannot be seen, cannot be felt,\n"+
				"    Cannot be heard, cannot be smelt.\n"+
				"    It lies behind stars and under hills,\n"+
				"    And empty holes it fills.\n"+
				"    It comes first and follows after,\n"+
				"    Ends life, kills laughter.\n");
			
			Console.WriteLine ("Choose wisely as a wrong answer can lead to horrific results:\n"+
				"1. The Myst\n" +
				"2. The Darkness\n" +
				"3. Life\n");
			ConsoleKey button = Console.ReadKey ().Key;	
			Console.WriteLine ();
			switch (button) 
			{
			case(ConsoleKey.D1):
				{
					///if number key 1 was pressed the text of the profession is now Warrior
					Console.Clear();
					Console.WriteLine ("The Sphinx gobbles the player up but after a few hours then the player wakes up in front of the Sphinx again\n");
					Console.ReadKey ();
					Top ();
					break;

				}

			case(ConsoleKey.D2):
				{
					///if number key 2 was pressed the text of the profession is now Rogue
					Console.WriteLine ("That was the correct answer and the Sphinx vanishes leaving chamber\n");
					Console.WriteLine ("Press 1 to go to lower floor");
					char key = Console.ReadKey ().KeyChar;
					if (key == '1') {
						Third_Floor.Third_Floor_ER ();
					}

					break;
				}

			case(ConsoleKey.D3):
				{
					///if number key 3 was pressed the text of the profession is now Wizard
					Console.WriteLine ("The Sphinx gobbles the player up but after a few hours then the player wakes up in front of the Sphinx again\n");
					Console.ReadKey ();

					Top ();
					break;
				}

			default:
				{
					///the player pressed a differnt key, so we restart the function from the beginning
					Console.Clear();
					Console.WriteLine ("INVALID INPUT - Press Enter to go back to menu\n\n");
					Console.ReadKey ();
					Top();
					break;
				}
			}
		}
		}

	}

