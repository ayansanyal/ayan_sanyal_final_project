﻿using System;

namespace Pyramid_of_Sphinx
{
	public class Pyramid
	{
		


		public static string m_Gender;
		//Item which will be used later in the game
		public static string m_Item = "";
		//main function that runs the program
		//basically a menu system with play, instructions and exit function
		public static void Main ()
		{
			Console.Clear ();
			Console.Title = "The Pyramid of the Sphinx";
			Console.BackgroundColor = ConsoleColor.Black;
			Console.ForegroundColor = ConsoleColor.Green;
			Console.WriteLine ("Press the corresponding number to select the option:\n");
			Console.WriteLine ("1. Play Game\n");
			Console.WriteLine ("2. Instructions\n");
			Console.WriteLine ("3. Exit Game\n");
			char key = Console.ReadKey ().KeyChar;
			if (key == '1') {
				Choice_Gender ();
				Console.WriteLine ("\nThe player enters from the top of the Pyramid. " +m_Gender + " feels a deep chill down the spine as\n" + m_Gender +
					" slides down the smooth marble wall\n");
				Game_Start.Top ();
			}
				
			if (key == '2') {
				Console.WriteLine ("Just follow the instructions as they come along and you'll be just fine\n");
				Console.ReadKey ();
				Main ();
			} else if (key == '3') {
				Console.WriteLine ();
				Environment.Exit (0);
			}
		}
		//Function for choosing gender of player
		static public void Choice_Gender()
		{
			Console.WriteLine ( "What is your player's gender?\n\n" +
				"(1) He's a man\n" +
				"(2) She's a woman\n" +
				"(3) They wish to remaind unidentified\n");

			ConsoleKey pressedKey = Console.ReadKey().Key;

			///possible gender adjectives
			switch (pressedKey) 
			{
			case(ConsoleKey.D1):
				{
					///if male player refered to as he
					m_Gender = "He";
					break;
				}

			case(ConsoleKey.D2):
				{
					///if female player refered to as she
					m_Gender = "She";
					break;
				}

			case(ConsoleKey.D3):
				{
					///if unidentified player refered to as they
					m_Gender = "They";
					break;
				}

			default:
				{
					///the player pressed a differnt key, so we restart the function from the beginning
					Console.Clear();
					Console.WriteLine ("INVALID INPUT - Press Enter to go back to menu\n\n");
					Console.ReadKey ();
					Choice_Gender();
					break;
				}
			}
		}

			
			}
	}


